// SPDX-FileCopyrightText: © 2021 Tech and Software Ltd.
// SPDX-License-Identifier: AGPL-3.0-only OR LicenseRef-uk.ltd.TechAndSoftware-1.0

const SPLASH = 'QIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECAsaMIEGDx8YIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQICxowg0N2bdmgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgLGjCBFvz9_6BAZQIECBAgQIECAig0NECBAgQIECBAgQIECAsaMcOD5tjyoGCBAgQIECBAgQICLhCgQIECBAgQIECBAgQICxowoTod79ArSEcHBAgQIECBAg0ITKBAgQcGCBAgQIECBAgLGjCBBgXIUCAyRXmlLBAgQIECBuZ4fPn___aoECBAgQIECBAaQIECBAgQIEBFAgQaTPDh8-f___-vXo9f9qgQIECBAgQIECBAgQIEBFAgQcOHz5____69ejRoECBAg__0CBAgQIECBAgQIECBAgQEUCL___r16NGXQIOHDh8-NCOD-3QIECBAgQIECBAgQIECBARQINf0ug-fPi9evRo0aBAgI6v6VAgQIECBAgQIECBAgQIEBFAgRf2hfBw4cOHD58-fPiAj-_oECBAgQIECBAgQIECBAgQEUCBBr-l0SNGjRo0CBAgQEcH9qgQIECBAgQIECBAgQIECBARQIECL-0Lr169ev-fPnxAR1f0KBAgQIECBAgQIAiBAgQIEBFAgQINf3hw4cOCBAgQIEBH-_QIECBAgQIECBAgCIECBAgQIEB1ARRL1-_____________7VAgOIECBAgQIECAIgQIECBAgQHUBNAgQf26BAgQIN_8ijRoECA4gQIECBAgQIAiBAgQIECA6gQE0CDV_QoECBAgRf2iBAgQIEBxAgQIECBAgCIECBAgQIDqBAgQE0aFAgQIECBAgQIECBAgQHECBAgQIECAIgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIBp0KgQIECBAgQIKmXZl6ZfHRBW05e-XkgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQKMalAyYMmKCplx6ECZBT35unfDyyoJnTIuQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA';
const HELP = 'OoECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA6RQIECBAgQYOHDhw4cOHDhw4cOHDhw4cOHBAgQIECBAgQIDo0igQIECBBqAy8vnFvw8siDHv3dOW_ZzI_2qBAgQIECBAgQIECBAgQIEHdAgQIECBAgQIECDjz9IECBAgQIECBAgQIECA6RQIECBAgQIl69evXr169evXr169evXr169KgQIECBAgQIDqBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgOhKvPKg6aMqDNv2bN_fTuzoNeXzzXILO_qgx4dyDDs570CA6E688qDzv68kHDDz6ZUHPRv5dMfXog15fPNB03oNm_DkXIDoFAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgOlNSNGjRo0aNGjRo0aNGjRo0aNGjRo0aNGjRo0aNGjR_2iA6U1BECBBsQIECCZvw5EHDDnyoECBAgQIECBAUQIECBB_aoDpTUEQIED9AgQIKWXtlw7ECBAgQIECBAgQIEBRAgQIEH9qgOlNQRAgQbUCBAgm6fCBAgQIECBAgQIECBAgQFECBAgQf2qA6U1BECBBzQIECCFv8ZciDfuX782ZAgQIECAogQIECBB_aoDpTUEQIEHBAgQIKGnxl2IOvDnjw7MqDfuX782ZAUQIEH9qgOlNQRAgQYkCBAghYcevPy39d2RBj37N_XkgQFECBAgQf2qA6U1BECBBrQIECCFsw49aDFhx68_Lf13ZECBAgQIECAp_aoDpTUEQIEGZAgQII2_d0Qc--npj0IECBAgQIEBRAgQIEH9qgOlNQRAgQekCBAgtZdyDbvyZUG_cv35syBAgQFECBAgQf2qA6U1BECBBnQIECCPy05EG_cv35syBAgQIECAogQIECBB_aoDpTUEQIEHhAgQIKWHdk37dPPKgQIECBAgQICiBAgQIEH9qgOlNQRAgQdECBAgqZefRBww58vNAgQIECBAgKIECBAgQf2qA6U1BECBBjQIECCHow8ueXog599PTHoQIECAogQIECBB_aoDpRV8-fPnz58-fPnz58-fPnz58-fPnz58-fPnz58-fPn9qgOoECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA';
const SMOOTH_PLUGIN_URL = 'https://cdn.jsdelivr.net/npm/@techandsoftware/teletext-plugin-smooth-mosaic@latest/dist/teletext-plugin-smooth-mosaic.min.js';

const LOOKUP_NYBBLE_TO_CHARSET = {
    '000': 'g0_latin__english',
    '001': 'g0_latin__german',
    '010': 'g0_latin__swedish_finnish_hungarian',
    '011': 'g0_latin__italian',
    '100': 'g0_latin__french',
    '101': 'g0_latin',
    '110': 'g0_hebrew',
    '111': 'g0_cyrillic__russian_bulgarian',
};

// maps charsets to their index in the CHARACTER_SETS array
const LOOKUP_CHARSET_TO_INDEX = {
    'g0_latin__english' : 2,
    'g0_latin__german' : 5,
    'g0_latin__swedish_finnish_hungarian' : 12,
    'g0_latin__italian' : 6,
    'g0_latin__french' : 4,
    'g0_latin' : 0,
    'g0_hebrew' : 19,
    'g0_cyrillic__russian_bulgarian' : 15,
};

import { Teletext, Level } from '@techandsoftware/teletext';
import { ttxcaster } from '@techandsoftware/teletext-caster';
import Modal from 'bootstrap/js/src/modal';
import 'bootstrap/js/src/offcanvas';

// The following arrays are used to cycle through values
const FONTS = ['Source Code Pro', 'Bedstead', 'Unscii', 'native', 'Ubuntu', 'sans-serif', 'monospace'];
const CHARACTER_SETS = [
    'g0_latin',
    'g0_latin__czech_slovak',
    'g0_latin__english',
    'g0_latin__estonian',
    'g0_latin__french',
    'g0_latin__german',
    'g0_latin__italian',
    'g0_latin__latvian_lithuanian',
    'g0_latin__polish',
    'g0_latin__portuguese_spanish',
    'g0_latin__romanian',
    'g0_latin__serbian_croatian_slovenian',
    'g0_latin__swedish_finnish_hungarian',
    'g0_latin__turkish',
    'g0_greek',
    'g0_cyrillic__russian_bulgarian',
    'g0_cyrillic__serbian_croatian',
    'g0_cyrillic__ukranian',
    'g0_arabic',
    'g0_hebrew',
];
const G2_CHARACTER_SETS = [
    'g2_greek',
    'g2_cyrillic',
    'g2_arabic',
    'g2_latin',
];
const VIEWS = ['classic__graphic-for-mosaic', 'classic__font-for-mosaic'];

class ViewerApp {
    constructor() {
        this._ttx = Teletext();
        this._ttx.addTo('#teletextscreen');
        this._ttx.loadPageFromEncodedString(SPLASH);

        this.KEY_EVENTS = {
            '?': 'ttx.reveal',
            'm': 'ttx.mix',
            's': 'ttx.subtitlemode',
        };

        this._fontIndex = 0;
        this._g2CharSetIndex = 0;
        this._viewIndex = 0;
        this._smoothPluginIsLoaded = false;
        this._packedPage = null;
        this._boxed = false;
        this._grid = false;
        this.ui = {};
        this.uistate = {};

        ttxcaster.available.attach(() => this._castAvailable.call(this));
        ttxcaster.castStateChanged.attach(() => this._castStateChanged.call(this));

        this._initEventListeners();

        // update page state on reload
        const loaded = this._loadUrl(window.location, true);
        this._ttx.setLevel(Level[document.querySelector('#levelSelect').value]);
        this._ttx.setFont(FONTS[document.querySelector('#fontSelect').value]);
        if (!loaded) this._ttx.setDefaultG0Charset(CHARACTER_SETS[document.querySelector('#primaryG0Select').value], true);
        if (document.querySelector('#upscaleSwitch').checked) this._loadSmoothPlugin();
        if (document.querySelector('#gridSwitch').checked) {
            this._ttx.toggleGrid();
            this._grid = true;
        }

        // ignore boxed state if switch is set on reload
        if (document.querySelector('#boxedSwitch').checked) {
            document.querySelector('#boxedSwitch').checked = false;
        }
    }

    _initEventListeners() {
        window.addEventListener('keydown', e => handleKeyPress.call(this, e));

        window.addEventListener('DOMContentLoaded', () => {
            window.addEventListener('popstate', e => this._handleHistoryPopState(e));
            window.addEventListener('paste', e => {
                if (this.uistate.pasteDialogOpen) return;
                const pasted = (e.clipboardData || window.clipboardData).getData('text')
                this._loadUrl(pasted);
            });
            document.getElementById('revealButton').addEventListener('click', () => {
                window.dispatchEvent(new Event('ttx.reveal'));
                ttxcaster.toggleReveal();
            });
            document.getElementById('mixButton').addEventListener('click', () => {
                window.dispatchEvent(new Event('ttx.mix'));
                ttxcaster.toggleMixMode();
            });
            document.querySelector('#helpicon').addEventListener('click', () => this._showHelp());
            document.querySelector('#openicon').addEventListener('click', () => {
                this.ui.pasteDialog.show();
                document.querySelector('#urlTextarea').focus();
            });
            document.querySelector('#upscaleSwitch').addEventListener('change', e => {
                const checked = e.target.checked;
                if (checked) {
                    if (this._smoothPluginIsLoaded) return;
                    if (this._viewIndex == 0) this._loadSmoothPlugin();
                } else {
                    if (!this._smoothPluginIsLoaded) return;
                    this._unloadSmoothPlugin();
                }
            });
            document.querySelector('#backgroundicon').addEventListener('click', () => this._generateBackground());
            document.querySelector('#blackbackgroundicon').addEventListener('click', () => this._blackBackground());
            document.querySelector('#randomiseicon').addEventListener('click', () => this._ttx.showRandomisedPage());

            const pasteDialogEl = document.getElementById('pasteDialog');
            pasteDialogEl.addEventListener('show.bs.modal', () => this.uistate.pasteDialogOpen = true);
            pasteDialogEl.addEventListener('hidden.bs.modal', () => this.uistate.pasteDialogOpen = false);
            this.ui.pasteDialog = new Modal(pasteDialogEl);
            this.ui.urlTextarea = document.getElementById('urlTextarea');

            document.querySelector('#loadUrlButton').addEventListener('click', () => {
                const urlVal = this.ui.urlTextarea.value;
                this.ui.pasteDialog.hide();
                this.ui.urlTextarea.value = '';        
                this._loadUrl(urlVal);
            });

            document.querySelector('#levelSelect').addEventListener('input', e => {
                this._ttx.setLevel(Level[e.target.value])
            });

            document.querySelector('#boxedSwitch').addEventListener('click', e => {
                const checked = e.target.checked;
                if (checked && !this._boxed) {
                    this._ttx.toggleBoxMode();
                    this._boxed = true;
                } else if (!checked && this._boxed) {
                    this._ttx.toggleBoxMode();
                    this._boxed = false;
                }
            });

            document.querySelector('#gridSwitch').addEventListener('click', e => {
                const checked = e.target.checked;
                if (checked && !this._grid) {
                    this._ttx.toggleGrid();
                    this._grid = true;
                } else if (!checked && this._grid) {
                    this._ttx.toggleGrid();
                    this._grid = false;
                }
            });

            document.querySelector('#primaryG0Select').addEventListener('input', e => {
                this._ttx.setDefaultG0Charset(CHARACTER_SETS[e.target.value], true);
                this._charSetIndex = e.target.value;
            });

            document.querySelector('#fontSelect').addEventListener('input', e => {
                this._ttx.setFont(FONTS[e.target.value]);
                this._fontIndex = e.target.value;
            });
        });

    }

    _loadUrl(urlVal, isPageLoad = false) {
        try {
            const url = new URL(urlVal);
            const data = getDataFromHash(url.hash);
            if (data) {
                this._ttx.setDefaultG0Charset(data.charset);
                this._ttx.loadPageFromEncodedString(data.frame);
                document.querySelector('#primaryG0Select').value = LOOKUP_CHARSET_TO_INDEX[data.charset];
                if (!isPageLoad) this._updateHistoryState(url.hash);
                this._packedPage = data.frame;
                this._castFrame();
                return true;
            }
        } catch (e) {
            console.error('E147 loadUrl failed to load page from url:', e.name, ':', e.message);
        }
        return false;
    }

    _updateHistoryState(hash) {
        const state = {};
        const title = '';
        const url = new URL(window.location);
        url.hash = hash;

        history.pushState(state, title, url);
    }

    _handleHistoryPopState() {
        const data = getDataFromHash(window.location.hash);
        if (data) {
            this._ttx.setDefaultG0Charset(data.charset);
            this._ttx.loadPageFromEncodedString(data.frame);
            document.querySelector('#primaryG0Select').value = LOOKUP_CHARSET_TO_INDEX[data.charset];
        }
    }

    _toggleZenMode() {
        document.body.classList.toggle('zen');
    }

    _generateBackground() {
        const hue = Math.floor((Math.random() * 360));
        const deg = Math.floor((Math.random() * 360));
        const bg = `linear-gradient(${deg}deg, hsl(${hue} 100% 7%) 0%, hsl(${hue} 83% 52%) 86%, hsl(${hue} 100% 85%) 100%)`;
        document.body.style.background = bg;
    }

    _blackBackground() {
        document.body.style.background = 'black';
    }

    _showHelp() {
        this._ttx.loadPageFromEncodedString(HELP);
    }

    _castStateChanged() {
        const state = ttxcaster.getCastState();
        const castEl = document.querySelector('#castOuter');
        switch (state) {
            case 'NO_DEVICES_AVAILABLE':
                castEl.title = "Cast to Chromecast - no devices available";
                castEl.style.cursor = 'default';
                break;

            case 'NOT_CONNECTED':
                castEl.title = "Cast to Chromecast";
                castEl.style.cursor = 'pointer';
                break;

            case 'CONNECTING':
                break;

            case 'CONNECTED':
                this._castFrame();
                if (this._smoothPluginIsLoaded) ttxcaster.setSmoothMosaics();
                break;
        }
    }

    _castAvailable() {
        document.querySelector('#castOuter').style.display = 'inline-block';
    }

    _castFrame() {
        const casterDisplay = {
            defaultG0Charset: CHARACTER_SETS[document.querySelector('#primaryG0Select').value],
            packed: this._packedPage
        };
        ttxcaster.display(casterDisplay);
    }

    _setUpscaleSwitch(value) {
        document.querySelector('#upscaleSwitch').checked = value;
    }

    async _loadSmoothPlugin() {
        try {
            const module = await import(SMOOTH_PLUGIN_URL);
            this._ttx.registerViewPlugin(module.SmoothMosaicPlugin);
            this._smoothPluginIsLoaded = true;
            ttxcaster.setSmoothMosaics();
        } catch (e) {
            console.error('ViewerApp: Failed to use smooth mosaic plugin: import failed:', e.message);
            this._smoothPluginIsLoaded = false;
        }
    }

    _unloadSmoothPlugin() {
        this._ttx.setView(VIEWS[this._viewIndex]); // resetting the view removes the plugin
        this._smoothPluginIsLoaded = false;
        ttxcaster.setBlockMosaics();
    }

    _rotateCharset() {
        let charsetIndex = document.querySelector('#primaryG0Select').value;
        charsetIndex++;
        if (charsetIndex == CHARACTER_SETS.length) charsetIndex = 0;
        this._ttx.setDefaultG0Charset(CHARACTER_SETS[charsetIndex], true);
        document.querySelector('#primaryG0Select').value = charsetIndex;
    }
}

function getDataFromHash(hashSegment) {
    const matches = hashSegment.match('^#(?<nybble>.):(?<frame>[A-Za-z0-9-_=]+)(:.+)?');
    if (matches && matches.groups) {
        return {
            charset: getCharsetFromNybble(Number(`0x${matches.groups.nybble}`)),
            frame: matches.groups.frame
        };
    }
    return null;
}

function getCharsetFromNybble(num) {
    const bits = num.toString(2).padStart(4, '0').slice(1, 4);
    return LOOKUP_NYBBLE_TO_CHARSET[bits];
}

async function handleKeyPress(e) {
    if (this.uistate.pasteDialogOpen) return;

    if (e.ctrlKey || e.altKey || e.metaKey) return;
    switch (e.key) {
        case '?': // reveal
            window.dispatchEvent(new Event(this.KEY_EVENTS[e.key]));
            // Reveal, mix and boxed could use the API on the teletext instance instead of dispatching an event.
            // The event is useful if the app code needs to remain decoupled from the teletext instance
            break;
        case 'm': // mix
            window.dispatchEvent(new Event(this.KEY_EVENTS[e.key]));
            break;
        case 's': // boxed mode (subtitles)
            window.dispatchEvent(new Event(this.KEY_EVENTS[e.key]));
            this._boxed = !this._boxed;
            document.querySelector('#boxedSwitch').checked = this._boxed;
            break;
        case 't':
            this._ttx.showTestPage();
            break;
        case 'x':
            this._ttx.showRandomisedPage();
            break;
        case 'g':
            this._ttx.toggleGrid();
            this._grid = !this._grid;
            document.querySelector('#gridSwitch').checked = this._grid;
            break;

        case 'f': // rotate through fonts
            this._fontIndex++;
            if (this._fontIndex == FONTS.length) this._fontIndex = 0;
            console.debug('setting font to', FONTS[this._fontIndex]);
            this._ttx.setFont(FONTS[this._fontIndex]);
            document.querySelector('#fontSelect').value = this._fontIndex;
            break;

        case 'c': // charset - rotate through g0 character sets
            this._rotateCharset();
            break;

        case 'v': // switch views which changes the mosaic rendering method
            this._viewIndex++;
            if (this._viewIndex == VIEWS.length) this._viewIndex = 0;
            this._ttx.setView(VIEWS[this._viewIndex]);
            this._smoothPluginIsLoaded = false;
            break;

        case 'p': // load or remove the plugin to smooth mosaic graphics
            if (this._smoothPluginIsLoaded) {
                this._unloadSmoothPlugin();
                this._setUpscaleSwitch(false);
                ttxcaster.setBlockMosaics();
            } else if (this._viewIndex == 0) {  // plugin works on the graphical mosaic view
                this._loadSmoothPlugin();
                this._setUpscaleSwitch(true);
            }
            break;

        case 'l': // load
            this.ui.pasteDialog.show();
            e.preventDefault();
            document.querySelector('#urlTextarea').focus();
            break;

        case 'o': // encOding - set g2 set
            this._g2CharSetIndex++;
            if (this._g2CharSetIndex == G2_CHARACTER_SETS.length) this._g2CharSetIndex = 0;
            this._ttx.setG2Charset(G2_CHARACTER_SETS[this._g2CharSetIndex], true);
            break;

        case 'z': // zen
            this._toggleZenMode();
            break;

        case 'b': // background
            this._generateBackground();
            break;

        case 'k': // blacK
            this._blackBackground();
            break;

        case 'h':
            this._showHelp();
            break;

        default:
    }
}

// run the app
new ViewerApp();
