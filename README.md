<!-- SPDX-FileCopyrightText: © 2021 Tech and Software Ltd. -->
<!-- SPDX-License-Identifier: AGPL-3.0-only OR LicenseRef-uk.ltd.TechAndSoftware-1.0 -->

Browser-based teletext page viewer using vector graphics. Wrapper app around [@techandsoftware/teletext](https://www.npmjs.com/package/@techandsoftware/teletext) with a basic UI.

* Shows teletext pages using vector graphics in a scalable page
* Loads pages from edit.tf or compatible URLs
* Mosaic graphics upscaling using a pixel-art upscaling algorithm
* Cast to Chromecast

# Development

## Local dev

```
npm install
npm run dev
```

Build goes to `public/dist`

Starts a server.

## Prod build

```
npm run build
```

# Credits

* Edit.tf hash fragment format from https://github.com/rawles/edit.tf
* Test pages from https://archive.teletextarchaeologist.org/
* Bedstead font by bjh21 - http://bjh21.me.uk/bedstead/
* Unscii font by Viznut - http://viznut.fi/unscii/ 
* Mosaic upscaling uses https://www.npmjs.com/package/@techandsoftware/teletext-plugin-smooth-mosaic which uses https://www.npmjs.com/package/js-hqx , licensed under GNU LGPL

# Licensing

Follows [reuse](https://reuse.software) conventions. 

Licensed with AGPL-3.0-only OR LicenseRef-uk.ltd.TechAndSoftware-1.0.  See LICENSES directory and SPDX tags per file.  Fonts have their own licenses, see the .license files in the fonts directory.
