// SPDX-FileCopyrightText: © 2021 Tech and Software Ltd.
// SPDX-License-Identifier: AGPL-3.0-only OR LicenseRef-uk.ltd.TechAndSoftware-1.0

import resolve from '@rollup/plugin-node-resolve';
import { terser } from "rollup-plugin-terser";

const OUTPUT_BANNER = `// SPDX${''}-FileCopyrightText: (c) 2021 Tech and Software Ltd.
// SPDX${''}-FileCopyrightText: (c) 2017 dosaygo
// SPDX${''}-License-Identifier: AGPL-3.0-only OR LicenseRef-uk.ltd.TechAndSoftware-1.0
// LicenseRef-uk.ltd.TechAndSoftware-1.0 refers to https://tech-and-software.ltd.uk/LICENSES/LicenseRef-uk.ltd.TechAndSoftware-1.0.txt
// Contains @techandsoftware/teletext https://www.npmjs.com/package/@techandsoftware/teletext
// Contains @techandsoftware/teletext-caster https://www.npmjs.com/package/@techandsoftware/teletext-caster
//
// Contains part of Bootstrap (https://getbootstrap.com/)
//   Copyright 2011-2021 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
//   Licensed under MIT (https://github.com/twbs/bootstrap/blob/main/LICENSE)`;

export default {
  input: {
    ViewerApp: 'src/ViewerApp.js',
  },
  output: {
    entryFileNames: '[name].js',
    dir: 'public/dist',
    format: 'es',
  },
  plugins: [
    terser({
      ecma: 2016,
      toplevel: true,
      compress: {
        drop_console: true,
        passes: 2,
        pure_getters: true,
        unsafe: true,
        unsafe_symbols: true,
        unsafe_arrows: true,
      },
      mangle: {
        properties: {
          regex: /^_.+|.+_$/, // ^_.+ for private methods, and .+_$ for internal methods. We don't match _ as that's used in the character mappings
        },
      },
      format: {
        preamble: OUTPUT_BANNER
      }
    }),
    resolve(),
  ],
};
